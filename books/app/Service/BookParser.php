<?php

namespace App\Service;

use Illuminate\Contracts\Filesystem\FileNotFoundException;

require_once __DIR__ . '/../../../../../wp-load.php';

class BookParser
{
    public function doWork($filePath)
    {
        $content = simplexml_load_file($filePath);

        if (!$content) {
            throw new FileNotFoundException("Unable read file {$filePath}");
        }

        foreach ($content as $item) {
            $this->createOrUpdate($item);
        }
    }

    protected function createOrUpdate($item)
    {
        $postData = [
            'post_type' => 'book',
            'post_title' => $item->title->__toString()
        ];

        $customFields = [
            'isbn' => $item->ISBN->__toString(),
            'description' => $item->description->__toString()
        ];

        $searchData = [
            'post_type' => 'book',
            'meta_key' => 'isbn',
            'meta_value' => $item->ISBN->__toString(),
            'post_status' => 'any'
        ];

        $post = get_posts($searchData);

        $postId = ([] === $post) ? wp_insert_post($postData) : $post[0]->ID;

        $this->addAttachment($item->image->__toString(), $postId);
        $this->updateCustomFields($postId, $customFields);
    }

    protected function updateCustomFields($postId, $fieldsWithData)
    {
        foreach ($fieldsWithData as $field => $value) {
            update_field($field, $value, $postId);
        }
    }

    protected function addAttachment($url, $postId) {
        $upload = wp_upload_bits( basename($url), null, file_get_contents($url) );

        if( !empty( $upload['error'] ) ) {
            return false;
        }

        $filePath = $upload['file'];
        $fileName = basename( $filePath );
        $fileType = wp_check_filetype( $fileName, null );
        $attachmentTitle = sanitize_file_name( pathinfo( $fileName, PATHINFO_FILENAME ) );
        $wpUploadDir = wp_upload_dir();
        $postInfo = array(
            'guid'           => $wpUploadDir['url'] . '/' . $fileName,
            'post_mime_type' => $fileType['type'],
            'post_title'     => $attachmentTitle,
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        $attachId = wp_insert_attachment( $postInfo, $filePath, $postId);
        $attachData = wp_generate_attachment_metadata( $attachId, $filePath );

        wp_update_attachment_metadata( $attachId,  $attachData );

        return $attachId;
    }
}