<?php

namespace App\CustomFields;

use StoutLogic\AcfBuilder\FieldsBuilder;

class Book
{
    public function get()
    {
        $book = new FieldsBuilder('book');

        $book->addField('isbn', 'text', $this->getIsbnArgs())
            ->addImage('cover', $this->getCoverArgs())
            ->addTextarea('description', $this->getDescriptionArgs());

        $book->setLocation('post_type', '==', 'book');

        return $book->build();
    }

    protected function getIsbnArgs()
    {
        return [
            'key' => 'isbn',
            'label' => 'ISBN',
            'name' => 'isbn',
            'required' => true,
        ];
    }

    protected function getCoverArgs()
    {
        return [];
    }

    protected function getDescriptionArgs()
    {
        return [];
    }
}