<?php

namespace App\Command;

use App\Service\BookParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends Command
{
    private $parser;

    public function __construct()
    {
        parent::__construct();
        $this->parser = new BookParser();
    }

    protected function configure()
    {
        $this->setName('books:import')
            ->addOption('file', 'f', InputOption::VALUE_REQUIRED, 'absolute path to file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getOption('file');

        $this->parser->doWork($path);
    }
}